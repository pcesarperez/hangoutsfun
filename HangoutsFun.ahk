﻿; HangoutsFun.ahk
;
; This script is intended to cause rage and dispair. Use at your own peril.
; The rage is conveyed through a series of Hangouts Easter Eggs.
; @see https://www.techjunkie.com/google-hangouts-easter-eggs/


; General configuration.
#SingleInstance force
Menu, Tray, Icon, HangoutsFun.ico


; A shitload of rainbow-colored ponies passing by.
:*?::ponies::
Loop, 10 {
    Send /ponies{Enter}
}
return


; A shitload of enraged bystanders out for blood.
:*?::pitchforks::
Loop, 10 {
    Send /pitchforks{Enter}
}
return


; Simulation of the effects of LSD consumption.
:*?::lsd::
Loop, 10 {
    Send /bikeshed{Enter}
}
return